/*
 * LMS1xx.h
 *
 *  Created on: 09-08-2010
 *  Author: Konrad Banachowicz
 ***************************************************************************
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc., 59 Temple Place,                                    *
 *   Suite 330, Boston, MA  02111-1307  USA                                *
 *                                                                         *
 ***************************************************************************/

#ifndef LMS1XX_H_
#define LMS1XX_H_

#include "LMS1xx_structs.h"
#include <string.h>
#include <stdint.h>

typedef enum
{
  disconnected = -1,
  undefined = 0,
  initialisation = 1,
  configuration = 2,
  idle = 3,
  rotated = 4,
  in_preparation = 5,
  ready = 6,
  ready_for_measurement = 7
} status_t;

typedef struct LMS1xx LMS1xx;

  /*!
  * @brief Connect to LMS1xx.
  * @param ip   LMS1xx IP address.
  * @param port LMS1xx port number.
  */
  LMS1xx* LMS1xx_connect(const char* ip, int port);

  /*!
  * @brief Disconnect from LMS1xx device & free [s].
  */
  void LMS1xx_disconnect(LMS1xx *s);

  /*!
  * @brief Get status of connection.
  * @returns connected or not.
  */
  int LMS1xx_isConnected(LMS1xx *s);

  /*!
  * @brief Start measurements.
  * After receiving this command LMS1xx unit starts spinning laser and measuring.
  * @returns 0 on success, -1 on error (disconnection)
  */
  int LMS1xx_startMeas(LMS1xx *s);

  /*!
  * @brief Stop measurements.
  * After receiving this command LMS1xx unit stop spinning laser and measuring.
  * @returns 0 on success, -1 on error (disconnection)
  */
  int LMS1xx_stopMeas(LMS1xx *s);

  /*!
  * @brief Get current status of LMS1xx device.
  * @returns status of LMS1xx device.
  */
  status_t LMS1xx_queryStatus(LMS1xx *s);

  /*!
  * @brief Log into LMS1xx unit.
  * Increase privilege level, giving ability to change device configuration.
  * @returns 0 on success, -1 on error (disconnection)
  */
  int LMS1xx_login(LMS1xx *s);

  /*!
  * @brief Get current scan configuration.
  * Get scan configuration and return it in [cfg].
  * - scanning frequency.
  * - scanning resolution.
  * - start angle.
  * - stop angle.
  * @returns 0 on success, -1 on error (disconnection)
  */
  int LMS1xx_getScanCfg(LMS1xx *s, scanCfg *cfg);

  /*!b
  * @brief Set scan configuration.
  * Scan configuration :
  * - scanning frequency.
  * - scanning resolution.
  * - start angle.
  * - stop angle.
  * @param cfg structure containing scan configuration.
  * @returns 0 on success, -1 on error (disconnection)
  */
  int LMS1xx_setScanCfg(LMS1xx *s, const scanCfg cfg);

  /*!
  * @brief Set scan output range.
  * Scan configuration :
  * - scanning frequency.
  * - scanning resolution.
  * - start angle.
  * - stop angle.
  * @param cfg structure containing scan configuration.
  * @returns 0 on success, -1 on error (disconnection)
  */
  int LMS1xx_setScanOutputRange(LMS1xx *s, const scanOutputRange cfg);

  /*!
  * @brief Set scan data configuration.
  * Set format of scan message returned by device.
  * @param cfg structure containing scan data configuration.
  * @returns 0 on success, -1 on error (disconnection)
  */
  int LMS1xx_setScanDataCfg(LMS1xx *s, const scanDataCfg cfg);

  /*!
  * @brief Get current output range configuration.
  * Get output range configuration and return in [outputRange]
  * - scanning resolution.
  * - start angle.
  * - stop angle.
  * @returns 0 on success, -1 on error (disconnection)
  */
  int LMS1xx_getScanOutputRange(LMS1xx *s, scanOutputRange *outputRange);

  /*!
  * @brief Start or stop continuous data acquisition.
  * After reception of this command device start or stop
  * continuous data stream containing scan messages
  * that can be obtained using LMS1xx_getScanData.
  * @param start 1 : start 0 : stop
  * @returns 0 on success, -1 on error (disconnection)
  */
  int LMS1xx_scanContinous(LMS1xx *s, int start);

  /*!
  * @brief Receive a single scan message.
  * Either from a continous stream or after a poll.
  * @return 1 if scan was read successfully,
  *         0 if nothing was read (timeout), or invalid data was read,
  *        -1 on error (disconnection)
  */
  int LMS1xx_getScanData(LMS1xx *s, scanData* scan_data);

  /*!
  * @brief Poll a single scan message.
  * After this command the result can be obtained using LMS1xx_getScanData.
  * @returns 0 on success, -1 on error (disconnection)
  */
  int LMS1xx_pollScanData(LMS1xx *s);

  /*!
  * @brief Save data permanently.
  * Parameters are saved in the EEPROM of the LMS and will also be available after the device is switched off and on again.
  * @returns 0 on success, -1 on error (disconnection)
  */
  int LMS1xx_saveConfig(LMS1xx *s);

  /*!
  * @brief The device is returned to the measurement mode after configuration.
  * @returns 0 on success, -1 on error (disconnection)
  */
  int LMS1xx_startDevice(LMS1xx *s);

  /*!
  * @brief Factory reset.
  * @returns 0 on success, -1 on error (disconnection)
  */
  int LMS1xx_factoryReset(LMS1xx *s);

#endif /* LMS1XX_H_ */
