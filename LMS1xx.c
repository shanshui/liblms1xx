/*
 * LMS1xx.cpp
 *
 *  Created on: 09-08-2010
 *  Author: Konrad Banachowicz
 ***************************************************************************
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc., 59 Temple Place,                                    *
 *   Suite 330, Boston, MA  02111-1307  USA                                *
 *                                                                         *
 ***************************************************************************/

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>

#include "LMS1xx.h"
#include "lms_buffer.h"

typedef struct LMS1xx {
  int connected;
  LMSBuffer buffer;
  int socket_fd;
} LMS1xx;

LMS1xx* LMS1xx_connect(const char *ip, int port)
{
  LMS1xx *s = malloc (sizeof(LMS1xx));
  bzero(s, sizeof(*s));

  logDebug("Creating non-blocking socket.");
  s->connected = 0;
  s->socket_fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (s->socket_fd) {
    int flags =1;
    setsockopt(s->socket_fd, SOL_TCP, TCP_NODELAY, (void *)&flags, sizeof(flags));

    struct sockaddr_in stSockAddr;
    stSockAddr.sin_family = PF_INET;
    stSockAddr.sin_port = htons(port);
    inet_pton(AF_INET, ip, &stSockAddr.sin_addr);

    logDebug("Connecting socket to laser.");
    int ret = connect(s->socket_fd, (struct sockaddr *) &stSockAddr, sizeof(stSockAddr));

    if (ret == 0) {
      s->connected = 1;
      logDebug("Connected succeeded.");
    } else {
      s->connected = 0;
      close(s->socket_fd);
      logError("%s: could not connect to %s:%d: %s",__func__,ip,port,strerror(errno));
    }
  }
  return s;
}

int close_conn (LMS1xx *s) {
  close(s->socket_fd);
  s->socket_fd = 0;
  s->connected = 0;
  return -1;
}

void LMS1xx_disconnect(LMS1xx *s)
{
  if (s->connected)
    close_conn(s);
  free (s);
}

int LMS1xx_isConnected(LMS1xx *s)
{
  return s->connected;
}

int LMS1xx_startMeas(LMS1xx *s)
{
  char buf[100];
  sprintf(buf, "%c%s%c", 0x02, "sMN LMCstartmeas", 0x03);

  if (write(s->socket_fd, buf, strlen(buf)) < 0)
    return close_conn(s);

  int len = read(s->socket_fd, buf, 100);
  if (len < 0)
    return close_conn(s);

  if (buf[0] != 0x02)
    logWarn("invalid packet recieved");
  buf[len] = 0;
  logDebug("RX: %s", buf);
  return 0;
}

int LMS1xx_stopMeas(LMS1xx *s)
{
  char buf[100];
  sprintf(buf, "%c%s%c", 0x02, "sMN LMCstopmeas", 0x03);

  if (write(s->socket_fd, buf, strlen(buf)) < 0)
    return close_conn(s);

  int len = read(s->socket_fd, buf, 100);
  if (len < 0)
    return close_conn(s);

  if (buf[0] != 0x02)
    logWarn("invalid packet recieved");
  buf[len] = 0;
  logDebug("RX: %s", buf);
  return 0;
}

status_t LMS1xx_queryStatus(LMS1xx *s)
{
  char buf[100];
  sprintf(buf, "%c%s%c", 0x02, "sRN STlms", 0x03);

  if (write(s->socket_fd, buf, strlen(buf)) < 0)
    return close_conn(s);

  int len = read(s->socket_fd, buf, 100);
  if (len < 0)
    return close_conn(s);

  if (buf[0] != 0x02)
    logWarn("invalid packet recieved");
  buf[len] = 0;
  logDebug("RX: %s", buf);

  int ret;
  sscanf((buf + 10), "%d", &ret);

  return (status_t) ret;
}

int LMS1xx_login(LMS1xx *s)
{
  char buf[100];
  int result;
  sprintf(buf, "%c%s%c", 0x02, "sMN SetAccessMode 03 F4724744", 0x03);

  fd_set readset;
  struct timeval timeout;

  do { //loop until data is available to read
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;

    if (write(s->socket_fd, buf, strlen(buf)) < 0)
      return close_conn(s);

    FD_ZERO(&readset);
    FD_SET(s->socket_fd, &readset);
    result = select(s->socket_fd + 1, &readset, NULL, NULL, &timeout);
  } while (result <= 0);

  int len = read(s->socket_fd, buf, 100);
  if (len < 0)
    return close_conn(s);

  if (buf[0] != 0x02)
    logWarn("invalid packet recieved");
  buf[len] = 0;
  logDebug("RX: %s", buf);
  return 0;
}

int LMS1xx_getScanCfg(LMS1xx *s, scanCfg *cfg)
{
  char buf[100];
  sprintf(buf, "%c%s%c", 0x02, "sRN LMPscancfg", 0x03);

  if (write(s->socket_fd, buf, strlen(buf)) < 0)
    return close_conn(s);

  int len = read(s->socket_fd, buf, 100);
  if (len < 0)
    return close_conn(s);

  if (buf[0] != 0x02)
    logWarn("invalid packet recieved");
  buf[len] = 0;
  logDebug("RX: %s", buf);

  sscanf(buf + 1, "%*s %*s %X %*d %X %X %X",
         &cfg->scanningFrequency,
         &cfg->angleResolution,
         &cfg->startAngle,
         &cfg->stopAngle);
  return 0;
}

int LMS1xx_setScanCfg(LMS1xx *s, const scanCfg cfg)
{
  char buf[100];
  sprintf(buf, "%c%s %X +1 %X %X %X%c", 0x02, "sMN mLMPsetscancfg",
          cfg.scanningFrequency, cfg.angleResolution,
          -450000, 2250000, 0x03);
  logDebug("TX: %s", buf);

  if (write(s->socket_fd, buf, strlen(buf)) < 0)
    return close_conn(s);

  int len = read(s->socket_fd, buf, 100);
  if (len < 0)
    return close_conn(s);

  buf[len - 1] = 0;
  return 0;
}

int LMS1xx_setScanDataCfg(LMS1xx *s, const scanDataCfg cfg)
{
  char buf[100];
  sprintf(buf, "%c%s %02X 00 %d %d 0 %02X 00 %d %d 0 %d +%d%c", 0x02,
          "sWN LMDscandatacfg", cfg.outputChannel, cfg.remission ? 1 : 0,
          cfg.resolution, cfg.encoder, cfg.position ? 1 : 0,
          cfg.deviceName ? 1 : 0, cfg.timestamp ? 1 : 0, cfg.outputInterval, 0x03);
  logDebug("TX: %s", buf);

  if (write(s->socket_fd, buf, strlen(buf)) < 0)
    return close_conn(s);

  int len = read(s->socket_fd, buf, 100);
  if (len < 0)
    return close_conn(s);

  buf[len - 1] = 0;
  return 0;
}

int LMS1xx_setScanOutputRange(LMS1xx *s, const scanOutputRange cfg)
{
  char buf[100];
  sprintf(buf, "%c%s +1 %X %X %X%c", 0x02, "sWN LMPoutputRange",
          cfg.angleResolution, cfg.startAngle, cfg.stopAngle, 0x03);
  logDebug("TX: %s", buf);

  if (write(s->socket_fd, buf, strlen(buf)) < 0)
    return close_conn(s);

  int len = read(s->socket_fd, buf, 100);
  if (len < 0)
    return close_conn(s);

  buf[len - 1] = 0;
  return 0;
}

int LMS1xx_getScanOutputRange(LMS1xx *s, scanOutputRange *outputRange)
{
  char buf[100];
  sprintf(buf, "%c%s%c", 0x02, "sRN LMPoutputRange", 0x03);

  if (write(s->socket_fd, buf, strlen(buf)) < 0)
    return close_conn(s);

  int len = read(s->socket_fd, buf, 100);
  if (len < 0)
    return close_conn(s);

  sscanf(buf + 1, "%*s %*s %*d %X %X %X",
         &outputRange->angleResolution,
         &outputRange->startAngle,
         &outputRange->stopAngle);
  return 0;
}

int LMS1xx_scanContinous(LMS1xx *s, int start)
{
  char buf[100];
  sprintf(buf, "%c%s %d%c", 0x02, "sEN LMDscandata", start, 0x03);

  if (write(s->socket_fd, buf, strlen(buf)) < 0)
    return close_conn(s);

  int len = read(s->socket_fd, buf, 100);
  if (len < 0)
    return close_conn(s);

  if (buf[0] != 0x02)
    logError("invalid packet recieved");

  buf[len] = 0;
  logDebug("RX: %s", buf);
  return 0;
}

int LMS1xx_saveConfig(LMS1xx *s)
{
  char buf[100];
  sprintf(buf, "%c%s%c", 0x02, "sMN mEEwriteall", 0x03);

  if (write(s->socket_fd, buf, strlen(buf)) < 0)
    return close_conn(s);

  int len = read(s->socket_fd, buf, 100);
  if (len < 0)
    return close_conn(s);

  if (buf[0] != 0x02)
    logWarn("invalid packet recieved");
  buf[len] = 0;
  logDebug("RX: %s", buf);
  return 0;
}

int LMS1xx_startDevice(LMS1xx *s)
{
  char buf[100];
  sprintf(buf, "%c%s%c", 0x02, "sMN Run", 0x03);

  if (write(s->socket_fd, buf, strlen(buf)) < 0)
    return close_conn(s);

  int len = read(s->socket_fd, buf, 100);
  if (len < 0)
    return close_conn(s);

  if (buf[0] != 0x02)
    logWarn("invalid packet recieved");
  buf[len] = 0;
  logDebug("RX: %s", buf);
  return 0;
}

int LMS1xx_factoryReset(LMS1xx *s)
{
  char buf[100];
  sprintf(buf, "%c%s%c", 0x02, "sMN mSCloadfacdef", 0x03);

  if (write(s->socket_fd, buf, strlen(buf)) < 0)
    return close_conn(s);

  int len = read(s->socket_fd, buf, 100);
  if (len < 0)
    return close_conn(s);

  if (buf[0] != 0x02)
    logWarn("invalid packet recieved");
  buf[len] = 0;
  logDebug("RX: %s", buf);
  return 0;
}

int parseScanData(char* buffer, scanData* data)
{
  *data = (scanData){ 0 };

  char* tok = strtok(buffer, " "); //Type of command
  tok = strtok(NULL, " "); //Command
  tok = strtok(NULL, " "); //VersionNumber
  tok = strtok(NULL, " "); //DeviceNumber
  tok = strtok(NULL, " "); //Serial number
  tok = strtok(NULL, " "); //DeviceStatus
  tok = strtok(NULL, " "); //MessageCounter
  tok = strtok(NULL, " "); //ScanCounter
  tok = strtok(NULL, " "); //PowerUpDuration
  tok = strtok(NULL, " "); //TransmissionDuration
  tok = strtok(NULL, " "); //InputStatus
  tok = strtok(NULL, " "); //OutputStatus
  tok = strtok(NULL, " "); //ReservedByteA
  tok = strtok(NULL, " "); //ScanningFrequency
  tok = strtok(NULL, " "); //MeasurementFrequency
  tok = strtok(NULL, " ");
  tok = strtok(NULL, " ");
  tok = strtok(NULL, " ");
  tok = strtok(NULL, " "); //NumberEncoders
  int NumberEncoders;
  sscanf(tok, "%d", &NumberEncoders);
  for (int i = 0; i < NumberEncoders; i++)
  {
    tok = strtok(NULL, " "); //EncoderPosition
    tok = strtok(NULL, " "); //EncoderSpeed
  }

  tok = strtok(NULL, " "); //NumberChannels16Bit
  int NumberChannels16Bit;
  sscanf(tok, "%d", &NumberChannels16Bit);
  logDebug("NumberChannels16Bit : %d", NumberChannels16Bit);

  for (int i = 0; i < NumberChannels16Bit; i++)
  {
    int type = -1; // 0 DIST1 1 DIST2 2 RSSI1 3 RSSI2
    char content[6];
    tok = strtok(NULL, " "); //MeasuredDataContent
    sscanf(tok, "%s", content);
    if (!strcmp(content, "DIST1"))
    {
      type = 0;
    }
    else if (!strcmp(content, "DIST2"))
    {
      type = 1;
    }
    else if (!strcmp(content, "RSSI1"))
    {
      type = 2;
    }
    else if (!strcmp(content, "RSSI2"))
    {
      type = 3;
    }
    tok = strtok(NULL, " "); //ScalingFactor
    tok = strtok(NULL, " "); //ScalingOffset
    tok = strtok(NULL, " "); //Starting angle
    tok = strtok(NULL, " "); //Angular step width
    tok = strtok(NULL, " "); //NumberData
    int NumberData;
    sscanf(tok, "%X", &NumberData);
    logDebug("NumberData : %d", NumberData);

    if (type == 0)
    {
      data->dist_len1 = NumberData;
    }
    else if (type == 1)
    {
      data->dist_len2 = NumberData;
    }
    else if (type == 2)
    {
      data->rssi_len1 = NumberData;
    }
    else if (type == 3)
    {
      data->rssi_len2 = NumberData;
    }

    if (LMS1XX_MAX_DATA_LEN < data->dist_len1
        || LMS1XX_MAX_DATA_LEN < data->dist_len2
        || LMS1XX_MAX_DATA_LEN < data->rssi_len1
        || LMS1XX_MAX_DATA_LEN < data->rssi_len2) {
      logWarn("Invalid data length(s): dist1=%d, dist2=%d, rssi1=%d, rssi2=%d",
               data->dist_len1, data->dist_len2,
               data->rssi_len1, data->rssi_len1);
      return 0;
    }

    for (int i = 0; i < NumberData; i++)
    {
      int dat;
      tok = strtok(NULL, " "); //data
      sscanf(tok, "%X", &dat);

      if (type == 0)
      {
        data->dist1[i] = dat;
      }
      else if (type == 1)
      {
        data->dist2[i] = dat;
      }
      else if (type == 2)
      {
        data->rssi1[i] = dat;
      }
      else if (type == 3)
      {
        data->rssi2[i] = dat;
      }
    }
  }

  tok = strtok(NULL, " "); //NumberChannels8Bit
  int NumberChannels8Bit;
  sscanf(tok, "%d", &NumberChannels8Bit);
  logDebug("NumberChannels8Bit : %d\n", NumberChannels8Bit);

  for (int i = 0; i < NumberChannels8Bit; i++)
  {
    int type = -1;
    char content[6];
    tok = strtok(NULL, " "); //MeasuredDataContent
    sscanf(tok, "%s", content);
    if (!strcmp(content, "DIST1"))
    {
      type = 0;
    }
    else if (!strcmp(content, "DIST2"))
    {
      type = 1;
    }
    else if (!strcmp(content, "RSSI1"))
    {
      type = 2;
    }
    else if (!strcmp(content, "RSSI2"))
    {
      type = 3;
    }
    tok = strtok(NULL, " "); //ScalingFactor
    tok = strtok(NULL, " "); //ScalingOffset
    tok = strtok(NULL, " "); //Starting angle
    tok = strtok(NULL, " "); //Angular step width
    tok = strtok(NULL, " "); //NumberData
    int NumberData;
    sscanf(tok, "%X", &NumberData);
    logDebug("NumberData : %d\n", NumberData);

    if (type == 0)
    {
      data->dist_len1 = NumberData;
    }
    else if (type == 1)
    {
      data->dist_len2 = NumberData;
    }
    else if (type == 2)
    {
      data->rssi_len1 = NumberData;
    }
    else if (type == 3)
    {
      data->rssi_len2 = NumberData;
    }
    for (int i = 0; i < NumberData; i++)
    {
      int dat;
      tok = strtok(NULL, " "); //data
      sscanf(tok, "%X", &dat);

      if (type == 0)
      {
        data->dist1[i] = dat;
      }
      else if (type == 1)
      {
        data->dist2[i] = dat;
      }
      else if (type == 2)
      {
        data->rssi1[i] = dat;
      }
      else if (type == 3)
      {
        data->rssi2[i] = dat;
      }
    }
  }
  return 1;
}

int LMS1xx_getScanData(LMS1xx *s, scanData* scan_data)
{
  fd_set rfds;
  FD_ZERO(&rfds);
  FD_SET(s->socket_fd, &rfds);

  // Block a total of up to 100ms waiting for more data from the laser.
  while (1)
  {
    // Would be great to depend on linux's behaviour of updating the timeval, but unfortunately
    // that's non-POSIX (doesn't work on OS X, for example).
    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 100000;

    logDebug("entering select(%ld)", tv.tv_usec);
    int retval = select(s->socket_fd + 1, &rfds, NULL, NULL, &tv);
    logDebug("returned %d from select()", retval);
    if (0 < retval)
    {
      readFrom(&s->buffer, s->socket_fd);

      // Will return pointer if a complete message exists in the buffer,
      // otherwise will return null.
      char* buffer_data = getNextBuffer(&s->buffer);

      if (buffer_data)
      {
        int ret = parseScanData(buffer_data, scan_data);
        popLastBuffer(&s->buffer);
        return ret;
      }
    }
    else
    {
      // Select timed out or there was an fd error.
      return retval;
    }
  }
}

int LMS1xx_pollScanData(LMS1xx *s)
{
  char buf[100];
  sprintf(buf, "%c%s%c", 0x02, "sRN LMDscandata", 0x03);

  if (write(s->socket_fd, buf, strlen(buf)) < 0)
    return close_conn(s);
}
