/*
 * test.c
 *
 *  Created on: 09-08-2010
 *  Author: Konrad Banachowicz
 ***************************************************************************
 *   This library is free software; you can redistribute it and/or	   *
 *   modify it under the terms of the GNU Lesser General Public		   *
 *   License as published by the Free Software Foundation; either	   *
 *   version 2.1 of the License, or (at your option) any later version.	   *
 *									   *
 *   This library is distributed in the hope that it will be useful,	   *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of	   *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	   *
 *   Lesser General Public License for more details.			   *
 *									   *
 *   You should have received a copy of the GNU Lesser General Public	   *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc., 59 Temple Place,					   *
 *   Suite 330, Boston, MA  02111-1307	USA				   *
 *									   *
 ***************************************************************************/

#include "LMS1xx.h"

#include <stdio.h>
#include <unistd.h>

void getScanData (LMS1xx *laser)
{
    printf ("Get scan data:\n");
    scanData data;
    LMS1xx_getScanData(laser, &data);
    for (int n = 0; n < data.dist_len1; n++)
        fprintf (stderr, "dist1[%d] = %u\n", n, data.dist1[n]);
    for (int n = 0; n < data.dist_len2; n++)
        fprintf (stderr, "dist2[%d] = %u\n", n, data.dist2[n]);
    for (int n = 0; n < data.rssi_len1; n++)
        fprintf (stderr, "rssi1[%d] = %u\n", n, data.rssi1[n]);
    for (int n = 0; n < data.rssi_len2; n++)
        fprintf (stderr, "rssi2[%d] = %u\n", n, data.rssi2[n]);
    printf ("dist_len1 = %u\n", data.dist_len1);
    printf ("dist_len2 = %u\n", data.dist_len2);
    printf ("rssi_len1 = %u\n", data.rssi_len1);
    printf ("rssi_len2 = %u\n", data.rssi_len2);

}

int main()
{
    LMS1xx *laser = LMS1xx_connect("192.168.0.1", 2111);
    if(!LMS1xx_isConnected(laser))
	{
            printf("connection failed\n");
            return 0;
	}

    printf("Connected to laser\n");

    LMS1xx_login(laser);
    //LMS1xx_stopMeas(laser);
    //LMS1xx_factoryReset(laser);

    printf("Getting scan configuration ...\n");

    scanCfg c;
    LMS1xx_getScanCfg(laser, &c);
    printf("Scanning Frequency = %f Hz\n", c.scanningFrequency/100.0);
    printf("Angle Resolution = %f deg\n", c.angleResolution/10000.0);
    printf("Start Angle = %f\n", c.startAngle/10000.0);
    printf("Stop Angle = %f\n", c.stopAngle/10000.0);

    c.scanningFrequency = 2500;
    c.angleResolution = 5000;
    c.startAngle = -450000;
    c.stopAngle =  2250000;

    scanOutputRange r;
    LMS1xx_getScanOutputRange(laser, &r);
    printf("Output Angle Resolution = %f deg\n", r.angleResolution/10000.0);
    printf("Output Start Angle = %f\n", r.startAngle/10000.0);
    printf("Output Stop Angle = %f\n", r.stopAngle/10000.0);

    r.angleResolution = 5000;
    r.startAngle = 900000;
    r.stopAngle = 1800000;

    scanDataCfg cc;
    cc.deviceName = 0;
    cc.encoder = 0;
    cc.outputChannel = 1;
    cc.remission = 0;
    cc.resolution = 0;
    cc.position = 1;
    cc.outputInterval = 1;

    #if 0
    LMS1xx_setScanCfg(laser, c);
    LMS1xx_setScanDataCfg(laser, cc);
    LMS1xx_setScanOutputRange(laser, r);
    LMS1xx_saveConfig(laser);
    #endif

    printf("Start measurements ...\n");
    LMS1xx_startMeas(laser);

    LMS1xx_pollScanData(laser);
    getScanData(laser);

    printf("Wait for ready status ...\n");
    int ret = 0;
    while (ret != 7) {
        ret = LMS1xx_queryStatus(laser);
        printf("status : %d\n", ret);
        usleep(1000000);
    }
    printf("Laser ready\n");

    LMS1xx_pollScanData(laser);
    getScanData(laser);

    printf("Start continuous data transmission ...\n");
    LMS1xx_scanContinous(laser, 1);

    for(int i = 0; i < 3; i++) {
        printf ("Receive data sample %d ...\n", i);
        getScanData(laser);
    }

    printf("Stop continuous data transmission ...\n");
    LMS1xx_scanContinous(laser, 0);

    //LMS1xx_stopMeas(laser);

    printf("Disconnect from laser\n");
    LMS1xx_disconnect(laser);

    return 0;
}
