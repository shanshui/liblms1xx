{
  outputs = { self, nixpkgs }:
    let
      supportedSystems = [
        "x86_64-linux"
      ];
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
      nixpkgsFor =
        forAllSystems (system:
          import nixpkgs {
            inherit system;
            overlays = [ self.overlay ];
          });
    in
      {
        overlay = final: prev:
          with final;
          {
            liblms1xx =
              stdenv.mkDerivation rec {
                pname = "liblms1xx";
                version = "0.1.0";
                src = ./.;

                nativeBuildInputs = [ cmake ];
                meta = {
                  description = "Library for interfacing with SICK LMS1xx LIDAR";
                  license = lib.licenses.lgpl21Plus;
                };
              };
          };

        packages =
          forAllSystems (system:
            nixpkgsFor.${system});

        defaultPackage =
          forAllSystems (system:
            nixpkgsFor.${system}.liblms1xx);
      };
}
