/*
 * lms_buffer.h
 *
 *  Author: Mike Purvis <mpurvis@clearpathrobotics.com>
 ***************************************************************************
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Lesser General Public            *
 *   License as published by the Free Software Foundation; either          *
 *   version 2.1 of the License, or (at your option) any later version.    *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc., 59 Temple Place,                                    *
 *   Suite 330, Boston, MA  02111-1307  USA                                *
 *                                                                         *
 ***************************************************************************/

#ifndef LMS1XX_LMS_BUFFER_H_
#define LMS1XX_LMS_BUFFER_H_

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#define LMS_BUFFER_SIZE 50000
#define LMS_STX 0x02
#define LMS_ETX 0x03

#ifndef LOG_LEVEL
# define LOG_LEVEL 0
#endif

enum logLevels {
  ERROR = 0,
  WARN,
  INFO,
  DEBUG
};

#define log(level, msg, ...) \
  if (level <= LOG_LEVEL) { \
    switch (level) { \
      case ERROR: \
        fprintf (stderr, "[ERROR] "); break; \
      case WARN: \
        fprintf (stderr, "[WARN] "); break; \
      case INFO: \
        fprintf (stderr, "[INFO] "); break; \
      case DEBUG: \
        fprintf (stderr, "[DEBUG] "); break; \
    } \
    fprintf (stderr, msg, ##__VA_ARGS__); \
    fprintf (stderr, "\n"); \
  }

#define logError(msg, ...) log(ERROR, msg, ##__VA_ARGS__)
#define logWarn(msg, ...) log(WARN, msg, ##__VA_ARGS__)
#define logInform(msg, ...) log(INFO, msg, ##__VA_ARGS__)
#define logDebug(msg, ...) log(DEBUG, msg, ##__VA_ARGS__)

  typedef struct {
    char buffer[LMS_BUFFER_SIZE];
    uint16_t total_length;
    char* end_of_first_message;
  } LMSBuffer;

  void shiftBuffer(LMSBuffer *s, char* new_start)
  {
    // Shift back anything remaining in the buffer.
    uint16_t remaining_length = s->total_length - (new_start - s->buffer);

    if (remaining_length > 0)
    {
      memmove(s->buffer, new_start, remaining_length);
    }
    s->total_length = remaining_length;
  }

  void readFrom(LMSBuffer *s, int fd)
  {
    int ret = read(fd, s->buffer + s->total_length, sizeof(s->buffer) - s->total_length);

    if (ret > 0)
    {
      s->total_length += ret;
      logDebug("Read %d bytes from fd, total length is %d.", ret, s->total_length);
    }
    else
    {

      logWarn("Buffer read() returned error.");
    }
  }

  char* getNextBuffer(LMSBuffer *s)
  {
    if (s->total_length == 0)
    {
      // Buffer is empty, no scan data present.
      logDebug("Empty buffer, nothing to return.");
      return NULL;
    }

    // The objective is to have a message starting at the start of the buffer, so if that's not
    // the case, then we look for a start-of-message character and shift the buffer back, discarding
    // any characters in the middle.
    char* start_of_message = (char*)memchr(s->buffer, LMS_STX, s->total_length);
    if (start_of_message == NULL)
    {
      // None found, buffer reset.
      logWarn("No STX found, dropping %d bytes from buffer.", s->total_length);
      s->total_length = 0;
    }
    else if (s->buffer != start_of_message)
    {
      // Start of message found, ahead of the start of buffer. Therefore shift the buffer back.
      logWarn("Shifting buffer, dropping %ld bytes, %ld bytes remain.",
              (start_of_message - s->buffer), s->total_length - (start_of_message - s->buffer));
      shiftBuffer(s, start_of_message);
    }

    // Now look for the end of message character.
    s->end_of_first_message = (char*)memchr(s->buffer, LMS_ETX, s->total_length);
    if (s->end_of_first_message == NULL)
    {
      // No end of message found, therefore no message to parse and return.
      logDebug("No ETX found, nothing to return.");
      return NULL;
    }

    // Null-terminate buffer.
    *s->end_of_first_message = 0;
    return s->buffer;
  }

  void popLastBuffer(LMSBuffer *s)
  {
    if (s->end_of_first_message)
    {
      shiftBuffer(s, s->end_of_first_message + 1);
      s->end_of_first_message = NULL;
    }
  }

#endif  // LMS1XX_LMS_BUFFER_H_
